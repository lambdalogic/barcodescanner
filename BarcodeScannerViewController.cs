﻿using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using CoreGraphics;
using RestSharp;
using RestSharp.Deserializers;
using Foundation;
using UIKit;
using ZXing;
using ZXing.Mobile;
using Appracatappra.ActionComponents.ActionAlert;
using LineaProSdk;

namespace BarcodeScanner
{
	public partial class BarcodeScannerViewController : UIViewController
	{
		NSObject observer;
		UIButton buttonDefaultScan;
		public BarcodeScannerViewController (IntPtr handle) : base (handle)
		{
		}

		Linea LineaDevice { get; set; }
		LineaDispatcher LineaDispatcher { get; set; }

		readonly UIColor DisconnectedColor = UIColor.FromRGB (0x00, 0xab, 0xbd);
		readonly UIColor ConnectingColor = UIColor.FromRGB (255, 206, 3);
		readonly UIColor ConnectedColor = UIColor.FromRGB (0x00, 0xab, 0xbd);

		//readonly int [] BarcodeTone = new int [] { 1046, 80, 1397, 80 };

		UILabel ConnectionLabel { get; set; }


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			//Load my Settings
			observer = NSNotificationCenter.DefaultCenter.AddObserver ((NSString)"NSUserDefaultsDidChangeNotification", UpdateSettings);

			View.BackgroundColor = UIColor.FromRGB (0x00, 0xab, 0xbd);
			//Setup our button
			buttonDefaultScan = new UIButton (UIButtonType.System);
			//buttonDefaultScan.Frame = new RectangleF(20, 240, 280, 60);
			buttonDefaultScan.BackgroundColor = UIColor.White;
			buttonDefaultScan.Frame = new CGRect (20, View.Frame.Height - 328, View.Frame.Width - 40, 60);
			Console.WriteLine ("Frame Hight: " + View.Frame.Height + " Frame Width: " + View.Frame.Width);
			//buttonDefaultScan.TintColor = UIColor.FromRGB (0x00, 0x5D, 0x28);
			buttonDefaultScan.TintColor = View.BackgroundColor;
			//buttonDefaultScan.SetTitle("QR-Code scannen", UIControlState.Normal);
			buttonDefaultScan.Layer.CornerRadius = 6;
			buttonDefaultScan.SetAttributedTitle (new Foundation.NSAttributedString ("QR-Code scannen", new UIStringAttributes {
				//ForegroundColor = View.BackgroundColor,
			}), UIControlState.Normal);


			if (Settings.LineaPro == "0" || String.IsNullOrEmpty (Settings.LineaPro)) {
				buttonDefaultScan.TouchUpInside += async (sender, e) => {


					var options = new ZXing.Mobile.MobileBarcodeScanningOptions ();
						options.PossibleFormats = new List<ZXing.BarcodeFormat> () { 
							ZXing.BarcodeFormat.CODE_128, ZXing.BarcodeFormat.QR_CODE 
						};
						options.TryHarder = true;
						var scanner1 = new ZXing.Mobile.MobileBarcodeScanner ();
						var result1 = await scanner1.Scan (options, true);
				
						if (result1 != null)
					{	
						Console.WriteLine ("Scanned Barcode: " + result1.Text);
						string server = Settings.ServerAddress;
						//Console.WriteLine ("Server: " + server);
						string token = Settings.ServerToken;

					string result = result1.Text;
						//Console.WriteLine ("Access Token: " + token);

						HandleRestGetRequest (result, server, token); 
					}
				};


				this.View.AddSubview (buttonDefaultScan);
			
			}
			if (Settings.LineaPro == "1"){


				Console.WriteLine ("Screen Height: " + View.Frame.Height);
				ConnectionLabel = new UILabel (new CGRect (10, View.Frame.Height - 328, View.Frame.Width - 20, 40));
				//ConnectionLabel.BackgroundColor = View.BackgroundColor;
				ConnectionLabel.BackgroundColor = UIColor.Clear;
				ConnectionLabel.TextAlignment = UITextAlignment.Center;
				ConnectionLabel.AdjustsFontSizeToFitWidth = true;
				ConnectionLabel.Font = UIFont.BoldSystemFontOfSize (15);
				ConnectionLabel.Text = "Linea disconnected";
				ConnectionLabel.TextColor = UIColor.FromRGB (245, 245, 245);

				LineaDevice = Linea.SharedDevice;
				LineaDispatcher = new LineaDispatcher ();

				LineaDispatcher.ConnectionStateChanged += HandleConnectionStateChanged;
				LineaDispatcher.BarcodeScanned += HandleBarcodeScanned;

				LineaDevice.AddDelegate (LineaDispatcher);
				LineaDevice.Connect ();

				this.View.AddSubview (ConnectionLabel);

			}
				
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			UpdateSettings (null);
			Console.WriteLine ("With PrintBadge: " + Settings.PrintBadge);
			Console.WriteLine ("With Placement: " + Settings.Placement);
			Console.WriteLine ("Using LineaPro Barcode Scanner: " + Settings.LineaPro);
			Console.WriteLine ("With ProgrammePoint: " + Settings.ProgrammePointID);

			string server = Settings.ServerAddress;
			string token = Settings.ServerToken;
			if (!String.IsNullOrEmpty(Settings.ProgrammePointID)) {

				string url = server + "/api/rest/v1/";
				string ppid = Settings.ProgrammePointID;
				var client = new RestClient (url);
				var getrequest = new RestRequest (String.Format ("programmePoints/{0}", ppid));
				getrequest.AddHeader ("Accept", string.Format ("application/xml", ParameterType.HttpHeader));
				getrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));


				client.ExecuteAsync<programmePoint> (getrequest,getresponse => {
					RestSharp.Deserializers.XmlDeserializer deserial = new XmlDeserializer();
					var programmePoint = deserial.Deserialize<programmePoint>(getresponse); 
	
					string ppName_lines = "";


					if ( getresponse.ResponseStatus.ToString() == "Completed" ) {
						//foreach (char ppName in programmePoint.name){
						//if (getresponse.Data.name != "") {
							ppName_lines = programmePoint.name;	
			
							//Console.WriteLine ("Response-Status: " +getresponse.ResponseStatus.ToString());
							//Console.WriteLine ("ppName_lines: " +ppName_lines);

							//break;
					
							this.BeginInvokeOnMainThread(() =>
							{

								UILabel programmePointName;
								programmePointName = new UILabel
								{
									Text = ppName_lines,
									Frame = new CGRect(20, View.Frame.Height - 38, View.Frame.Width - 40, 30),
								//BackgroundColor = UIColor.White,
								//BackgroundColor = View.BackgroundColor,
								BackgroundColor = UIColor.Clear,
								//TextColor = View.BackgroundColor,
								TextColor = UIColor.White,
								Font = UIFont.SystemFontOfSize (18f),
								TextAlignment = UITextAlignment.Center,
								Lines = 0

							};

							programmePointName.Layer.MasksToBounds = true;
							programmePointName.Layer.CornerRadius = 6;
							programmePointName.AdjustsFontSizeToFitWidth = true;

							View.AddSubview (programmePointName);
						
							});	
							
						//}

					}
				
				});

			}

		//to use the placement setting there has to be a programmePointTyeId of 9 in the subsequent regasus masterdata
	    //insert into programme_point_type values( 9 , now() , null , 'en="Placement";de="Platzierung"', 'placement' );
		
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			if (observer != null) {
				NSNotificationCenter.DefaultCenter.RemoveObserver (observer);
				observer = null;
			}
		}

		void UpdateSettings (NSNotification obj)
		{
			Settings.SetupByPreferences();	
		}

		public class ParticipantDTO
		{
			

			public string id { get; set; }
			public string firstName { get; set; }
			public string lastName { get; set; }
			public string vip { get; set;}
			public List<customFieldValues> customFieldValue { get; set; }
		}


		public class customFieldValues {
			
			public string id { get; set; }
			public string text { get; set; }
		}



		public string Id { get; set; }


		public class programmePoint {
			public string typeID {get; set;}
			public string position { get; set;}
			public string meetingPoint{ get; set;}
			public string name{ get; set;}
		}

		public class programmeBookings {

			public List<programmeBookings> programmeBooking { get; set;} 
		
		}

		public class programmeBooking
		{
			public string id { get; set;}
			public string meetingPoint { get; set;}
			public DateTime startTime { get; set;}
			public DateTime endTime { get; set;}
			public string typeID { get; set;}
		}

		void HandleRestGetRequest(string result, string server, string token)
		{
			string url = server + "/api/rest/v1/";
			string ppid = Settings.ProgrammePointID;
			string print = Settings.PrintBadge;
			string placement = Settings.Placement;
			var client = new RestClient (url);
			var getrequest = new RestRequest (String.Format ("participants/barcode={0}", result));
			getrequest.AddHeader ("Accept", string.Format ("application/xml", ParameterType.HttpHeader));
			getrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));
			if (result != null)
				client.ExecuteAsync<ParticipantDTO> (getrequest, getresponse => {
					Console.WriteLine ("Get Participant Response: " +getresponse.StatusCode);
					Console.WriteLine ("Get Participant's Lastname: " +getresponse.Data.lastName );
					Console.WriteLine ("Get Participant's Lastname: " +getresponse.Data.firstName );
					Console.WriteLine ("Get Participant's VIP status: " +getresponse.Data.vip );

					//RestSharp.Deserializers.XmlDeserializer deserial = new XmlDeserializer();
					//
					//var legacyCustomFields = deserial.Deserialize<List<customFieldValues>>(getresponse);
					//
					//foreach ( var cf in legacyCustomFields) {
					//		if (legacyCustomFields[i].text.ToString() != "") {
					//
					//	string vip_placement = legacyCustomFields[5].id.ToString();

					//		}
					//}

					//Console.WriteLine("Wert " + vip_placement);
					//Console.WriteLine ("Get Participant's VIP's placement value: " +legacyCustomFields[5].text);

					if (getresponse.StatusCode.ToString() == "NotFound" ) {
						HandleScanResult(result);
					}

					else if ( !String.IsNullOrEmpty(ppid)) {
						string stringbody = "<lead><participantID>" + getresponse.Data.id + "</participantID><programmePointID>" +ppid + "</programmePointID></lead>";
						//Console.WriteLine ("Creating Lead for Programme Point ID: " +ppid);
						//Console.WriteLine ("Body String: " + stringbody);
						var postrequest = new RestRequest (string.Format ("leads"));
						postrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));
						postrequest.Method = Method.POST;
						postrequest.RequestFormat = DataFormat.Xml;
						postrequest.AddParameter ("application/xml; charset=utf-8", stringbody, ParameterType.RequestBody);
					
						if (getresponse.Data.id != null) {
							client.ExecuteAsync<ParticipantDTO> (postrequest,  postresponse =>  {

								if (postresponse.ResponseStatus.ToString () != "Error") {
									Console.WriteLine ("Post-Response-Code: " + postresponse.StatusCode.ToString());
									var query = from r in postresponse.Headers
											where r.Name.Equals ("X-Regasus-LeadResponse")
										select new {r.Value};

									//Console.WriteLine ("LINQ result direct: " + query.Single ().Value.ToString ());
									string leadresult = query.Single().Value.ToString();


									string msg = "";

									if (getresponse.StatusCode.ToString () != "NotFound" && leadresult == "1") {
										




										msg = "Name: " + getresponse.Data.lastName + "\nVorname: " + getresponse.Data.firstName;


										if (placement == "1") {
											var getbookingsrequest = new RestRequest (string.Format ("programmeBookings/benefitRecipientID={0}?withCancelations=no&withProgrammeOffering=no&withProgrammePoint=yes", getresponse.Data.id));
											getbookingsrequest.AddHeader ("Accept", string.Format ("application/xml", ParameterType.HttpHeader));
											getbookingsrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));
											client.ExecuteAsync<programmeBooking> ( getbookingsrequest, getbookingsresponse => {
												RestSharp.Deserializers.XmlDeserializer deserial = new XmlDeserializer();
												var programmeBookings = deserial.Deserialize<List<programmeBooking>>(getbookingsresponse);

												Console.WriteLine ("Bookings Response Code: " +getbookingsresponse.StatusCode);
												Console.WriteLine ("Bookings Response Status: " +getbookingsresponse.ResponseStatus);
												Console.WriteLine ("Bookings List element count: " +programmeBookings.Count);
												Console.WriteLine ("-----------------------------------------");
												Console.WriteLine ("Current Date and Time: " +DateTime.Now);
												Console.WriteLine ("-----------------------------------------");
								
												var bookings = from pb in programmeBookings 
														where 
													(pb.typeID == "9") 
													&& (pb.startTime.CompareTo(DateTime.Now) < 0) 
													&& (pb.endTime.CompareTo(DateTime.Now) > 0)
													orderby pb.id descending
													select pb;

												string placement_lines = "";

												if ( getbookingsresponse.ResponseStatus.ToString() == "Completed" ) {
												foreach (var pb in bookings) {
													if(pb.meetingPoint.Count() != 0 ){
														string[] getPlacementValue = bookings.First().meetingPoint.Split('|');
														Console.WriteLine("Placement found!");
															if ( getPlacementValue[1].ToString() != ""){
															placement_lines = " Platzierung:" + "\n " +getPlacementValue[0] + "\n " +getPlacementValue[1];
															}
															else 
															placement_lines = " Platzierung:" + "\n " +getPlacementValue[0];
														break;
														}
													}
												}

												this.BeginInvokeOnMainThread(() => {

													if (placement_lines != "") {
													UIView _view;
													_view = new UIView (new RectangleF (0, 0, 210, 100));


													UILabel _msg;
													_msg = new UILabel {
														Text = msg,
														Frame = new RectangleF(0, 0, 210, 60),
														BackgroundColor = UIColor.FromRGB(0x14, 0xEE , 0x42),
														TextColor = UIColor.Black,
														Font = UIFont.SystemFontOfSize(18f),
														TextAlignment = UITextAlignment.Left,
														Lines = 0

													};

													_msg.Layer.MasksToBounds = true;

													UILabel _placement;
													_placement = new UILabel {
														Text = placement_lines,
														Frame = new RectangleF(0, 60, 210, 70),
														BackgroundColor = UIColor.White,
														TextColor = UIColor.Black,
														Font = UIFont.SystemFontOfSize(18f),
														TextAlignment = UITextAlignment.Left,
														Lines = 0

													};

													_placement.Layer.MasksToBounds = true;
													_placement.Layer.CornerRadius = 6;
													_placement.AdjustsFontSizeToFitWidth = true;

													_view.AddSubviews (new UIView[]{_msg, _placement});

													var alertView = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, UIImage.FromFile ("green-check.png"), "Zutritt gestattet", _view, "OK" );

													alertView.appearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
													alertView.buttonAppearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
													alertView.appearance.titleSize = 18;
													alertView.appearance.descriptionSize = 15;
													alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
													alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
													alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
													alertView.buttonAppearanceHighlighted.titleSize = 18;

													alertView.Show();

													alertView.ButtonReleased += (button) => {
														alertView.Hide();
													};

													}
													else {
														Console.WriteLine ("keine Platzierung");
														var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("green-check.png"), "Zutritt gestattet", msg);
													
														alertView.appearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
														alertView.buttonAppearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
														alertView.appearance.titleSize = 18;
														alertView.appearance.descriptionSize = 15;
														alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
														alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
														alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
														alertView.buttonAppearanceHighlighted.titleSize = 18;

														alertView.Show();

													}


												});
											
										
											});

										};

									
										if (placement != "1") {
										this.BeginInvokeOnMainThread(() => {


												if ( getresponse.Data.vip == "true" ) { 
													UIView _view;
													_view = new UIView (new RectangleF (0, 0, 210, 80));

													UILabel _msg;
													_msg = new UILabel {
														Text = msg,
														Frame = new RectangleF(0, 0, 210, 60),
														//BackgroundColor = UIColor.White,
														TextColor = UIColor.Black,
														Font = UIFont.SystemFontOfSize(18f),
														TextAlignment = UITextAlignment.Left,
														Lines = 0

													};

													_msg.Layer.MasksToBounds = true;


													UILabel _vip;
													_vip = new UILabel {
														Text = "VIP: Ehrengast",
														Frame = new RectangleF(0, 60, 210, 50),
														BackgroundColor = UIColor.FromRGB(212, 175, 55),
														TextColor = UIColor.White,
														Font = UIFont.SystemFontOfSize(28f),
														TextAlignment = UITextAlignment.Center,
														Lines = 0

													};

													_vip.Layer.MasksToBounds = true;
													_vip.Layer.CornerRadius = 6;
													_vip.AdjustsFontSizeToFitWidth = true;




													_view.AddSubviews (new UIView[]{ _msg , _vip });

													var alertView = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, UIImage.FromFile ("green-check.png"), "Zutritt gestattet", _view, "OK" );

													alertView.appearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
													alertView.buttonAppearance.background = UIColor.FromRGB(0x14, 0xEE, 0x42);
													alertView.appearance.titleSize = 18;
													alertView.appearance.descriptionSize = 15;
													alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0x14, 0xEE, 0x42);
													alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0x14, 0xEE, 0x42)	;
													alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
													alertView.buttonAppearanceHighlighted.titleSize = 18;

													alertView.Show ();

													alertView.ButtonReleased += (button) => {
														alertView.Hide();
													};

												}

											
												else {
											var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("green-check.png"), "Zutritt gestattet", msg );

											alertView.appearance.background = UIColor.FromRGB(0x14, 0xEE , 0x42);
											alertView.buttonAppearance.background = UIColor.FromRGB(0x14, 0xEE, 0x42);
											alertView.appearance.titleSize = 18;
											alertView.appearance.descriptionSize = 15;
											alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0x14, 0xEE, 0x42);
											alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0x14, 0xEE, 0x42)	;
											alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
											alertView.buttonAppearanceHighlighted.titleSize = 18;

											alertView.Show ();
											
												}
											
												});

											}
										
										}
									if (leadresult == "-1") {
										msg = "Nachname: " + getresponse.Data.lastName + "\nVorname: " + getresponse.Data.firstName;
										this.BeginInvokeOnMainThread(() => {


											if ( getresponse.Data.vip == "true" ) { 
												UIView _view;
												_view = new UIView (new RectangleF (0, 0, 210, 80));

												UILabel _msg;
												_msg = new UILabel {
													Text = msg,
													Frame = new RectangleF(0, 0, 210, 60),
													//BackgroundColor = UIColor.White,
													TextColor = UIColor.Black,
													Font = UIFont.SystemFontOfSize(18f),
													TextAlignment = UITextAlignment.Left,
													Lines = 0

												};

												_msg.Layer.MasksToBounds = true;


												UILabel _vip;
												_vip = new UILabel {
													Text = "VIP: Ehrengast",
													Frame = new RectangleF(0, 60, 210, 50),
													BackgroundColor = UIColor.FromRGB(212, 175, 55),
													TextColor = UIColor.White,
													Font = UIFont.SystemFontOfSize(28f),
													TextAlignment = UITextAlignment.Center,
													Lines = 0

												};

												_vip.Layer.MasksToBounds = true;
												_vip.Layer.CornerRadius = 6;
												_vip.AdjustsFontSizeToFitWidth = true;

												_view.AddSubviews (new UIView[]{ _msg , _vip });

												var alertView = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, UIImage.FromFile ("red-check.png"), "Zutritt verweigert", _view, "OK" );

												alertView.appearance.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
												alertView.appearance.titleSize = 18;
												alertView.appearance.descriptionSize = 15;
												alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
												alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
												alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
												alertView.buttonAppearanceHighlighted.titleSize = 18;

												alertView.Show ();

												alertView.ButtonReleased += (button) => {
													alertView.Hide();
												};

											}

											else {
											var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("red-check.png"), "Zutritt verweigert", msg );

											alertView.appearance.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
											alertView.appearance.titleSize = 18;
											alertView.appearance.descriptionSize = 15;
											alertView.buttonAppearanceHighlighted.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
											alertView.buttonAppearanceTouched.background = UIColor.FromRGB(0xFF, 0x00, 0x00);
											alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
											alertView.buttonAppearanceHighlighted.titleSize = 18;

											alertView.Show ();
										
											}

										});
									}
									if (leadresult == "0") {
										msg = "Nachname: " + getresponse.Data.lastName + "\nVorname: " + getresponse.Data.firstName;
										this.BeginInvokeOnMainThread(() => {

											if ( getresponse.Data.vip == "true" ) { 
												UIView _view;
												_view = new UIView (new RectangleF (0, 0, 210, 80));

												UILabel _msg;
												_msg = new UILabel {
													Text = msg,
													Frame = new RectangleF(0, 0, 210, 60),
													//BackgroundColor = UIColor.White,
													TextColor = UIColor.Black,
													Font = UIFont.SystemFontOfSize(18f),
													TextAlignment = UITextAlignment.Left,
													Lines = 0

												};

												_msg.Layer.MasksToBounds = true;


												UILabel _vip;
												_vip = new UILabel {
													Text = "VIP: Ehrengast",
													Frame = new RectangleF(0, 60, 210, 50),
													BackgroundColor = UIColor.FromRGB(212, 175, 55),
													TextColor = UIColor.White,
													Font = UIFont.SystemFontOfSize(28f),
													TextAlignment = UITextAlignment.Center,
													Lines = 0

												};

												_vip.Layer.MasksToBounds = true;
												_vip.Layer.CornerRadius = 6;
												_vip.AdjustsFontSizeToFitWidth = true;




												_view.AddSubviews (new UIView[]{ _msg , _vip });

												var alertView = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, UIImage.FromFile ("green-check.png"), "Lead erfolgreich erzeugt", _view, "OK" );


												alertView.appearance.titleSize = 18;
												alertView.appearance.descriptionSize = 15;
												alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
												alertView.buttonAppearanceTouched.background = UIColor.White;
												alertView.buttonAppearanceHighlighted.titleSize = 18;

												alertView.Show ();

												alertView.ButtonReleased += (button) => {
													alertView.Hide();
												};

											}

											else {

											var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("green-check.png"), "Lead erfolgreich erzeugt", msg );

											alertView.appearance.titleSize = 18;
											alertView.appearance.descriptionSize = 15;
											alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
											alertView.buttonAppearanceTouched.background = UIColor.White;
											alertView.buttonAppearanceHighlighted.titleSize = 18;

											alertView.Show ();
										
											}	
										});
									}
								}
							});
						}
					}
						
					else if (String.IsNullOrEmpty(ppid) && print == "1") {
					string stringbody = "<badge><participantID>" + getresponse.Data.id + "</participantID></badge>";
					Console.WriteLine ("Creating Badge for Participant ID: " +getresponse.Data.id);
					Console.WriteLine ("Body String: " + stringbody);
					var postrequest = new RestRequest (string.Format ("badges"));
					postrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));
					postrequest.Method = Method.POST;
					postrequest.RequestFormat = DataFormat.Xml;
					postrequest.AddParameter ("application/xml; charset=utf-8", stringbody, ParameterType.RequestBody);
					
					if (getresponse.Data.id != null) {
						client.ExecuteAsync<ParticipantDTO> (postrequest,  postresponse =>  {
							if (postresponse.ResponseStatus.ToString () != "Error") {
								var query = from r in postresponse.Headers
								          where r.Name.Equals ("Location")
								          select new {r.Value};
							
								Console.WriteLine ("LINQ result direct: " + query.Single ().Value);
											
								string msg = "";
								if (getresponse.StatusCode.ToString () != "NotFound") {
									msg = "Participant: " + getresponse.Data.firstName + " " + getresponse.Data.lastName
									+ " with ID: " + getresponse.Data.id + " and Badge Location: " + query.Single ().Value;
									Console.WriteLine (msg);
									string badgeUrl = query.Single ().Value.ToString ();
									this.BeginInvokeOnMainThread(() => {

									PrintBadge ( badgeUrl , token );

									});
								}
							};
						 });
					  }
					}
						
					else	
					{
						string msg = "";
						msg = "Nachname: " + getresponse.Data.lastName + "\n" + "Vorname: " + getresponse.Data.firstName;
						this.InvokeOnMainThread(() => {
							

							if ( getresponse.Data.vip == "true" ) { 
								UIView _view;
								_view = new UIView (new RectangleF (0, 0, 210, 80));

								UILabel _msg;
								_msg = new UILabel {
									Text = msg,
									Frame = new RectangleF(0, 0, 210, 60),
									//BackgroundColor = UIColor.White,
									TextColor = UIColor.Black,
									Font = UIFont.SystemFontOfSize(18f),
									TextAlignment = UITextAlignment.Left,
									Lines = 0

								};

								_msg.Layer.MasksToBounds = true;


								UILabel _vip;
								_vip = new UILabel {
									Text = "VIP: Ehrengast",
									Frame = new RectangleF(0, 60, 210, 50),
									BackgroundColor = UIColor.FromRGB(212, 175, 55),
									TextColor = UIColor.White,
									Font = UIFont.SystemFontOfSize(28f),
									TextAlignment = UITextAlignment.Center,
									Lines = 0

								};

								_vip.Layer.MasksToBounds = true;
								_vip.Layer.CornerRadius = 6;
								_vip.AdjustsFontSizeToFitWidth = true;

								_view.AddSubviews (new UIView[]{ _msg , _vip });

								var alertView = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, UIImage.FromFile ("regasus-57x57.png"), "Teilnehmerdaten", _view, "OK" );

								alertView.appearance.titleSize = 18;
								alertView.appearance.descriptionSize = 15;
								alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
								alertView.buttonAppearanceTouched.background = UIColor.White;
								alertView.buttonAppearanceHighlighted.titleSize = 18;

								alertView.Show ();
							
								alertView.ButtonReleased += (button) => {
									alertView.Hide();
								};
																					
							}

							else {

							var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("regasus-57x57.png"), "Teilnehmerdaten", msg );
							
							alertView.appearance.titleSize = 18;
							alertView.appearance.descriptionSize = 15;
							alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
							alertView.buttonAppearanceTouched.background = UIColor.White;
							alertView.buttonAppearanceHighlighted.titleSize = 18;

							alertView.Show ();
							
							}
						});
					}
						
				});
		}
			
		void HandleScanResult(string result)

		{

			string msg = "";
		
			if (result != null && !string.IsNullOrEmpty(result))

				msg = "QR-Code passt zu keinem Teilnehmer! \n\n" + result;

			this.InvokeOnMainThread(() => {

				var alertView = UIActionAlert.ShowAlertOK (UIImage.FromFile ("ActionAlert_57.png"), "Fehlerhafter QR-Code", msg );

				alertView.appearance.titleSize = 18;
				alertView.appearance.descriptionSize = 15;
				alertView.buttonAppearanceHighlighted.titleColor = UIColor.Black;
				alertView.buttonAppearanceTouched.background = UIColor.White;
				alertView.buttonAppearanceHighlighted.titleSize = 18;

				alertView.Show ();

			});
		
		}


		void getProgrammePointName(string server, string token)
		{
			string url = server + "/api/rest/v1/";
			string ppid = Settings.ProgrammePointID;
			var client = new RestClient (url);
			var getrequest = new RestRequest (String.Format ("programmePoints/{0}", ppid));
			getrequest.AddHeader ("Accept", string.Format ("application/xml", ParameterType.HttpHeader));
			getrequest.AddHeader ("Authorization", string.Format ("Bearer {0}", token, ParameterType.HttpHeader));
		
			client.ExecuteAsync<programmePoint> (getrequest, getresponse => {
				Console.WriteLine ("Name of ProgrammPoint: " +getresponse.Data.name);
				string ppName = getresponse.Data.name;
			});
	
		}

		void PrintBadge ( string badgeUrl , string token )
		{
			var printInfo = UIPrintInfo.PrintInfo;
			printInfo.OutputType = UIPrintInfoOutputType.General;
			printInfo.JobName = "Card Print Job";
	
			var printer = UIPrintInteractionController.SharedPrintController;
			printer.PrintInfo = printInfo;
			printer.ShowsPageRange = false;
			var request = HttpWebRequest.Create(string.Format(badgeUrl +"/document.pdf"));
			request.ContentType = "application/xml";
			request.Headers.Add("Authorization: Bearer "+token);
			request.Method = "GET";

			using (HttpWebResponse response = request.GetResponse () as HttpWebResponse) {
				if (response.StatusCode != HttpStatusCode.OK)
					Console.Out.WriteLine ("Error fetching data. Server returned status code: {0}", response.StatusCode);
				using (StreamReader reader = new StreamReader (response.GetResponseStream ())) {
					printer.PrintingItem = NSData.FromStream (reader.BaseStream);    
				}
			}

			printer.Present (true, (handler, completed, err) => {
				if (!completed && err != null){
					Console.WriteLine ("error");
				}
			});
		}


		void HandleConnectionStateChanged (LineaDelegate Dispatcher, ConnectionStateChangedEventArgs Arguments)
		{
			switch (Arguments.State) {
			case ConnStates.Disconnected:
				Console.WriteLine ("LineaPro disconnected.");
				View.BackgroundColor = DisconnectedColor;
					ConnectionLabel.Text = "Linea disconnected";
					ConnectionLabel.BackgroundColor = DisconnectedColor;
				break;
			case ConnStates.Connecting:
				Console.WriteLine ("LineaPro connecting...");
				View.BackgroundColor = ConnectingColor;
					ConnectionLabel.Text = "Linea connecting";
					ConnectionLabel.BackgroundColor = ConnectingColor;
				break;
			case ConnStates.Connected:
				Console.WriteLine ("LineaPro connected.");
				View.BackgroundColor = ConnectedColor;
					ConnectionLabel.Text = "Linea connected!";
					ConnectionLabel.BackgroundColor = ConnectedColor;
				break;

			}
		}


		void HandleBarcodeScanned (LineaDelegate Dispatcher, BarcodeScannedEventArgs Arguments)
		{
			Console.WriteLine (string.Format ("Barcode scanned: {0} ({1})", Arguments.Data, Arguments.BarcodeType));

			if (Arguments.Data != null) {
				string result = Arguments.Data;
			
			string server = Settings.ServerAddress;
			//Console.WriteLine ("Server: " + server);
			string token = Settings.ServerToken;


			HandleRestGetRequest (result, server, token);
			}
		}

	}
}