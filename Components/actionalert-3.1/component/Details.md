###About ActionAlert###

`ActionAlert` is a highly customizable, cross platform alert and notification system for iOS and Android. Present both modal and non-modal alerts, dialog boxes and notifications with a minimal of code to quickly inform the user of the state of a process or alert them of a situation that needs their attention in your mobile app.

`ActionAlert` makes it easy include images, activity indicators, progress bars, and interactive buttons (both enabled and disabled) to a notification, while providing for a maximum of code reuse across platforms. In many cases the same code used to display an `ActionAlert` on one platform can be used virtually unchanged on another which not only saves time, but improves code maintainability.


###Alert and Notification Types###

`ActionAlert` provides for four main alert and notification types which can be expanded on by optionally adding titles, descriptions, icons, buttons (such as _OK_ or _Cancel_) or any combination of the above. The base types are:

* **Default** - A standard alert type that can have an icon, title and/or description. The **Default** type has a fixed width of 312 pixels and a flexable height based on its content.
* **ActivityAlert** - Displays a "spinning gear" _ActivityIndicator_ on iOS or a "spinning circle" _ProgressBar_ on Android along with a title and/or description. The **ActivityAlert** type has a fixed width of 312 pixels and a flexable height based on its content.
* **ActivityAlertMedium** - The same as the **ActivityAlert** type but only allows for a title and has a fixed width of 123 pixels.
* **ProgressAlert** - Like the **Default** type it can have an icon, title and/or description however it includes a progress bar at the bottom of the notification. Also like the **Default** type, it has a fixed width of 312 pixels and a flexable height based on its content.
* **Subview** - Like the **Default* type but it replaces the description with any custom subview of your choosing.


###Default Locations and User Draggable

`ActionAlert` supports three default vertical locations: **Top**, **Middle** and **Bottom** with the alert centered horizontally. Set the default location to **Custom** and you can place the `ActionAlert` anywhere on screen via code.

Allow the user to move the alert by setting its _dragable_ property to _true_ and optionally set constraints on its x and y coordinates to limit its movement to a range of positions or lock it into a given location. By default the x coordinate is locked on an iPhone or iPod Touch device.


###Events###

`ActionAlert` defines the following events that you can monitor and respond to:

- Touched
- Moved
- Released
- ButtonTouched
- ButtonReleased
- OverlayTouched - For modal alerts, the user has tapped outside of the `ActionAlert`'s bounds

In addition individual `ActionAlertButtons` attached to an `ActionAlert` have **Touched** and **Released** events that can be responded to directly.

###Appearance###

`ActionAlert` was designed to look and function the same across platforms, however you have full control over its look and feel. `ActionAlert` is fully customizable with user definable appearances for every element of its UI. You can make either *Square* or *Round Rectangle* alert boxes with user defined corner radius or optionally square off individual corners. 

###iOS 8 Support###

`ActionAlert` automatically selects the iOS 7 look and feel when running on an iOS 7 or greater device. Of course you can always adjust the results of this styling to better fit the look of the alert to your mobile app.

###Features###

`ActionAlert` includes a fully documented **API** with comments for every feature. The `ActionAlert`'s user interface is drawn with vectors and is fully resolution independent.

###iOS Examples###

Here are a few examples that show creating and displaying `ActionAlerts` quickly via static methods of the *UIActionAlert* class. More control is available by creating your own instance of *UIActionAlert* and setting it's properties directly:

```
using Appracatappra.ActionComponents.ActionAlert;
…

// Default Alert title and description only
UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
…

// Default Alert with icon, title, description and OK button
UIActionAlert.ShowAlertOK (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
…

// Default Alert with title, description and custom buttons
_alert = UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.","Cancel,Maybe,OK");

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
	UIActionAlert.ShowAlert(UIActionAlertLocation.Top, String.Format ("You tapped the {0} button.", button.title),"");
};
…

// Non-modal Activity Alert (spinning gear) with a title only that can be freely moved around the screen by the user
_alert = UIActionAlert.ShowActivityAlert ("Activity Alert", "", false);
_alert.draggable = true;
…

// Non-modal medium sized Activity Alert (spinning gear) with a title
_alert = UIActionAlert.ShowActivityAlertMedium ("Waiting...", false);
…

// Non-modal alert with a progress bar (set to 50%), title and description
_alert = UIActionAlert.ShowProgressAlert ("Progress Alert", "Displays an Alert for a process with a known length and gives feedback to the user.", false);
_alert.progressView.Progress = 0.5f;

```


###Android Examples###

And here are the same example `ActionAlerts` created in the **OnCreate** method of an Android **Activity**:

```
using Appracatappra.ActionComponents.ActionAlert;
…

// Default Alert title and description only
UIActionAlert.ShowAlert (this, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
…

// Default Alert with icon, title, description and OK button
UIActionAlert.ShowAlertOK (this, Resource.Drawable.ActionAlert_57, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
…

// Default Alert with title, description and custom buttons
_alert = UIActionAlert.ShowAlert (this, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.","Cancel,Maybe,OK");

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
	UIActionAlert.ShowAlert(this, UIActionAlertLocation.Top, String.Format ("You tapped the {0} button.", button.title),"");
};
…

// Non-modal Activity Alert (spinning circular progress bar) with a title only that can be freely moved around the screen by the user
_alert = UIActionAlert.ShowActivityAlert (this, "Activity Alert", "", false);
_alert.draggable = true;
…

// Non-modal medium sized Activity Alert (spinning circular progress bar) with a title
_alert = UIActionAlert.ShowActivityAlertMedium (this, "Waiting...", false);
…

// Non-modal alert with a progress bar (set to 50%), title and description
_alert = UIActionAlert.ShowProgressAlert (this, "Progress Alert", "Displays an Alert for a process with a known length and gives feedback to the user.", false);
_alert.progressView.Progress = 50;

```

_Note: The addition of the keyword **this** to the start of every `ActionAlert` call in the above code is a reference to the current Activity being run and, aside from the difference in referencing an icon on Adroid, is the only change made from the iOS version of the code._

##Trial Version##

The Trial version of `ActionAlert` is fully functional but includes a _Toast_ style popup. The fully licensed version removes this popup.

_Some screenshots created with [PlaceIt](http://placeit.breezi.com "PlaceIt by Breezi")._
