##ActionAlert Component##

###Getting Started with ActionAlerts###

To use an `ActionAlert` in your mobile application include the `ActionAlert.iOS` or `ActionAlert.Android` component and reference the following using statement in your C# code:

```
using Appracatappra.ActionComponents.ActionAlert;
``` 

###A Special Note  About Working with ActionTrays in Android###

When using an `ActionAlert` on the Android OS you must specify the context as the first parameter of the `ActionAlert`'s call. This is usually the **Activity** that the alert is being displayed in.

_Note: You are also responsible for redisplaying an `ActionAlert` in response to the device being rotated or the Activity being restarted._

###Displaying a Simple ActionAlert###

`ActionAlerts` were designed to be displayed with a minimum of code and the quickest and easiest way to create and display one is by calling one of the many preset *Static* methods of the *UIActionAlert* class such as:

**iOS Example**

```
// Default Alert with icon, title, description and OK button
UIActionAlert.ShowAlertOK (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
```

**Android Example**

```
// Default Alert with icon, title, description and OK button
UIActionAlert.ShowAlertOK (this, Resource.Drawable.ActionAlert_57, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
```

For more complete control over the `ActionAlert` create a new instance by calling one of its constructors and adjust its properties and settings manually via code.

###Adding Buttons to an ActionAlert###

All of the `ActionAlert` base types can be made interactive by adding touchable buttons that will be incorporated into the bottom edge of the alert. Several *Static* methods exist for creating standard buttons such as *OK* or *Cancel*:

**iOS Example**

```
_alert = UIActionAlert.ShowAlertOKCancel ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
```

**Android Example**

```
_alert = UIActionAlert.ShowAlertOKCancel (this, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
```

Or custom buttons can be created using:

**iOS Example**

```
_alert = UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.","Cancel,Maybe,OK");
```

**Android Example**

```
_alert = UIActionAlert.ShowAlert (this, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.","Cancel,Maybe,OK");
```

Where *"Cancel,Maybe,OK"* represents a comma separated list of button titles that will be created and added to the alert.

`ActionAlert` buttons can also be enabled or disabled by adjusting the `Enabled` property of the `UIActionAlertButton` class. The `appearanceDisabled` property controls the appearance of the button when it is disabled.

###Responding to User Interaction###

`ActionAlert` defines several events that can be responded to such as _Touched_, _Moved_, _Released_, _ButtonTouched_, _ButtonReleased_ or _OverlayTouched_. The following is an example of handling a button being pressed on an `ActionAlert`:

```
//Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
	UIActionAlert.ShowAlert(UIActionAlertLocation.Top, String.Format ("You tapped the {0} button.", button.title),"");
};
```

###Using Dragable###

`ActionView` feature built-in drag handling with optional constraints on the X and/or Y axis. In the following example we will make an alert draggable, lock it's Y coordinate in place and allow the X coordinate to be drug within a given range:

```
// Set alert to be draggable and apply limits to it's movement
_alert.draggable = true;
_alert.xConstraint.constraintType = UIActionViewDragConstraintType.Constrained;
_alert.xConstraint.minimumValue = 140f;
_alert.xConstraint.maximumValue = 920f;
_alert.yConstraint.constraintType = UIActionViewDragConstraintType.Locked;
```

The drag controls work exactly the same on Android as well.

###Adjust an ActionAlert's Appearance###

`ActionAlerts` were designed to be totally customizable. Here is an example of adjusting an alerts color and squaring off one of its corners:

**iOS Example**

```
// Create an alert and customize it's appearance
_alert = UIActionAlert.ShowAlert (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert is Customizable", "You can 'square off' one or more of the corners and ajust all of the colors and styles by using properties of the alert.", "No,Yes");
_alert.appearance.background = UIColor.Orange;
_alert.buttonAppearance.background = UIColor.Orange;
_alert.buttonAppearanceHighlighted.titleColor = UIColor.Orange;
_alert.appearance.roundBottomLeftCorner = false;

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
};
```

**Android Example**

```
// Create an alert and customize it's appearance
_alert = UIActionAlert.ShowAlert (this, Resource.Drawable.ActionAlert_57, "ActionAlert is Customizable", "You can 'square off' one or more of the corners and ajust all of the colors and styles by using properties of the alert.", "No,Yes");
_alert.appearance.background = Color.Orange;
_alert.buttonAppearance.background = Color.Orange;
_alert.buttonAppearanceHighlighted.titleColor = Color.Orange;
_alert.appearance.roundBottomLeftCorner = false;

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
};
```

In addition `ActionAlerts` support iOS 7's design language and styling by calling the _Flatten_ method:

**iOS Example**

```
// iOS 7 Style
_alert = UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.", "Cancel,Maybe,OK");
_alert.Flatten ();
```
_NOTE: If you are running on an iOS 7 device, the Flatten method will automatically be called for you._

**Android Example**

```
// iOS 7 Style
_alert = UIActionAlert.ShowAlert (this, "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.", "Cancel,Maybe,OK");
_alert.Flatten ();
```
_NOTE: Flatten was added to the Android version to assist in cross platform developement._

###Custom Subview Alert###
`ActionAlert` now supports attaching a custom subview to the alert in place of the description area. Here is an example of adding a text edit box to an alert:

** iOS Example **

```
// Build a custom view for this alert
UITextField field1 = new UITextField (new RectangleF (13, 41, 278, 29)) {
	BorderStyle = UITextBorderStyle.RoundedRect,
	TextColor = UIColor.DarkGray,
	Font = UIFont.SystemFontOfSize (14f),
	Placeholder = "Name",
	Text = "DefaultValue",
	BackgroundColor = UIColor.White,
	AutocorrectionType = UITextAutocorrectionType.No,
	KeyboardType = UIKeyboardType.Default,
	ReturnKeyType = UIReturnKeyType.Done,
	ClearButtonMode = UITextFieldViewMode.WhileEditing,
	Tag = 0,
	Enabled = true
};
field1.ShouldReturn = delegate (UITextField field){
	field.ResignFirstResponder ();
	return true;
};
field1.ShouldEndEditing= delegate (UITextField field){
	field.ResignFirstResponder ();
	return true;
};

// Create the new alert and attach the new field
_alert = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, "Custom Subview", field1, "Cancel,OK");
_alert.Show ();

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	field1.ResignFirstResponder ();
	_alert.Hide();
};

```

**Android Example**

```
// Create subview
var lp = new RelativeLayout.LayoutParams (278, 50);
var flp = new RelativeLayout.LayoutParams (278, 50);
var rl = new RelativeLayout (this);
var field1 = new EditText (this);

// Initialize subview
rl.LayoutParameters = lp;

// Initialize field and add to subview
flp.TopMargin = 0;
flp.RightMargin = 0;
field1.LayoutParameters = flp;
field1.Text = "Default Value";
rl.AddView (field1);

// Create alert
_alert = new UIActionAlert (this, UIActionAlertType.Subview, UIActionAlertLocation.Middle, "Custom Subview", rl, "Cancel,OK");
_alert.Show ();

// Respond to any button being tapped
_alert.ButtonReleased += (button) => {
	_alert.Hide();
};
```

##Examples##

For full examples of using `ActionAlerts` in your mobile application please see the _ActionAlertTest.iOS_ or _ActionAlertTest.Android_ example apps included with this component.

See the API documentation for `ActionAlert` for a complete list of features and their usage.


##Trial Version##

The Trial version of `ActionAlert` is fully functional but includes a _Toast_ style popup. The fully licensed version removes this popup.

## Other Resources

* [Action Components](http://appracatappra.com/actioncomponents)
* [Android API Documentation](http://appracatappra.com/api/android/)
* [iOS API Documentation](http://appracatappra.com/api/ios/)
* [Appracatappra Support Forum](http://appracatappra.com/forum/index.php)
* [Support](http://appracatappra.com/apis/component-support)