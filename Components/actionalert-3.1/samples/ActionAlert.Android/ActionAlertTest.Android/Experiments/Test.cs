using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ActionAlertTest.Android
{
	[Activity (Label = "TestLayout", Theme = "@android:style/Theme.NoTitleBar")]
	public class Test : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Test);


		}

	}
}

