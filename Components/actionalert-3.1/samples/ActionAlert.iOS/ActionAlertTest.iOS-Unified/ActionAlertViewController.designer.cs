// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;

namespace ActionAlert
{
	[Register ("ActionAlertViewController")]
	partial class ActionAlertViewController
	{
		[Outlet]
		UIKit.UIButton AlertButton { get; set; }

		[Outlet]
		UIKit.UIStepper AlertStepper { get; set; }

		[Outlet]
		UIKit.UILabel AlertTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AlertButton != null) {
				AlertButton.Dispose ();
				AlertButton = null;
			}

			if (AlertTitle != null) {
				AlertTitle.Dispose ();
				AlertTitle = null;
			}

			if (AlertStepper != null) {
				AlertStepper.Dispose ();
				AlertStepper = null;
			}
		}
	}
}
