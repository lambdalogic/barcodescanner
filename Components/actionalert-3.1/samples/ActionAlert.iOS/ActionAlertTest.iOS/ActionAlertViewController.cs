using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Appracatappra.ActionComponents.ActionAlert;

namespace ActionAlert
{
	public partial class ActionAlertViewController : UIViewController
	{
		#region Private Variables
		private UIActionAlert _alert;
		#endregion 

		#region Static Properties
		/// <summary>
		/// Gets a value indicating whether this <see cref="ActionAlert.ActionAlertViewController"/> user interface idiom is phone.
		/// </summary>
		/// <value><c>true</c> if user interface idiom is phone; otherwise, <c>false</c>.</value>
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}
		#endregion 

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ActionAlert.ActionAlertViewController"/> class.
		/// </summary>
		public ActionAlertViewController ()
			: base (UserInterfaceIdiomIsPhone ? "ActionAlertViewController_iPhone" : "ActionAlertViewController_iPad", null)
		{

		}
		#endregion 

		#region Private Methods
		/// <summary>
		/// Sets the alert title.
		/// </summary>
		private void SetAlertTitle() {
			string[] titles = {"Default", "Title Only", "Description Only", "Default with Icon", "OK", @"OK & Image", "OK/Cancel", "Custom Buttons", "Activity Alert", 
				"Activity Title Only", "Activity Desc Only", "Activity Cancel", "Medium Activity Alert", "Med Activity Cancel", "Progress Alert",
				"Progress Title", "Progress Desc", "Progress Icon", "Progress Cancel", "Progress Custom Btns", "iOS 7 Style", "Customized", "Custom Subview"};

			// Set new title
			AlertTitle.Text = titles[(int)AlertStepper.Value];
		}

		/// <summary>
		/// Shows the alert.
		/// </summary>
		private void ShowAlert(){

			// Close any existing alert
			if (_alert != null) {
				_alert.Hide ();
				_alert = null;
			}

			// Take action based on the stepper value
			switch ((int)AlertStepper.Value) {
			case 0:
				UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
				break;
			case 1:
				UIActionAlert.ShowAlert ("ActionAlert", "");
				break;
			case 2:
				UIActionAlert.ShowAlert ("", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
				break;
			case 3:
				UIActionAlert.ShowAlert (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
				break;
			case 4:
				UIActionAlert.ShowAlertOK ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
				break;
			case 5:
				UIActionAlert.ShowAlertOK (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");
				break;
			case 6:
				_alert = UIActionAlert.ShowAlertOKCancel ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.");

				//Respond to the OK button being tapped
				_alert.ButtonReleased += (button) => {
					if (button.title=="OK") {
						UIActionAlert.ShowAlert(UIActionAlertLocation.Top, "You tapped the OK button.","");
					}
				};
				break;
			case 7:
				_alert = UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.","Cancel,Maybe,OK");

				//Respond to any button being tapped
				_alert.ButtonReleased += (button) => {
					_alert.Hide();
					UIActionAlert.ShowAlert(UIActionAlertLocation.Top, String.Format ("You tapped the {0} button.", button.title),"");
				};
				break;
			case 8:
				_alert = UIActionAlert.ShowActivityAlert ("Activity Alert", "Displays a notification to the user that some long running process has started. This one is being displayed non-modal so the user can still interact with the app.",false);
				break;
			case 9:
				_alert = UIActionAlert.ShowActivityAlert ("Activity Alert", "",false);
				break;
			case 10:
				_alert = UIActionAlert.ShowActivityAlert ("", "Displays a notification to the user that some long running process has started. This alert has been set to be movable by the user.", false);
				_alert.draggable = true;
				break;
			case 11:
				_alert = UIActionAlert.ShowActivityAlertCancel ("Activity Alert", "Displays a notification to the user that some long running process has started and allows the user to cancel the process.",true);
				break;
			case 12:
				_alert = UIActionAlert.ShowActivityAlertMedium ("Waiting...", false);
				break;
			case 13:
				_alert = UIActionAlert.ShowActivityAlertMediumCancel ("Waiting...", true);
				break;
			case 14:
				_alert = UIActionAlert.ShowProgressAlert ("Progress Alert", "Displays an Alert for a process with a known length and gives feedback to the user.", false);
				_alert.progressView.Progress = 0.5f;
				break;
			case 15:
				_alert = UIActionAlert.ShowProgressAlert ("Waiting on Process...", "", false);
				_alert.progressView.Progress = 0.1f;
				break;
			case 16:
				_alert = UIActionAlert.ShowProgressAlert ("", "Displays an Alert for a process with a known length and gives feedback to the user.", false);
				_alert.progressView.Progress = 0.8f;
				break;
			case 17:
				_alert = UIActionAlert.ShowProgressAlert (UIImage.FromFile ("ActionAlert_57.png"), "Progress Alert", "Displays an Alert for a process with a known length and gives feedback to the user.", false);
				_alert.progressView.Progress = 0.8f;
				break;
			case 18:
				_alert = UIActionAlert.ShowProgressAlertCancel ("Progress Alert", "Displays an Alert for a process with a known length and allos the user to cancel the process.", false);
				_alert.progressView.Progress = 0.2f;
				break;
			case 19:
				_alert = UIActionAlert.ShowProgressAlert ("Progress Alert", "Displays an Alert for a process with a known length and allos the user to cancel the process.", "Abort,Pause", true);
				_alert.progressView.Progress = 0.3f;

				//Respond to any button being tapped
				_alert.ButtonReleased += (button) => {
					_alert.Hide();
				};
				break;
			case 20:
				_alert = UIActionAlert.ShowAlert ("ActionAlert", "A cross platform Alert, Dialog and Notification system for iOS and Android.", "Cancel,Maybe,OK");
				_alert.Flatten ();

				//Respond to any button being tapped
				_alert.ButtonReleased += (button) => {
					_alert.Hide();
				};
				break;
			case 21:
				_alert = UIActionAlert.ShowAlert (UIImage.FromFile ("ActionAlert_57.png"), "ActionAlert is Customizable", "You can 'square off' one or more of the corners and ajust all of the colors and styles by using properties of the alert.", "No,Yes");
				_alert.appearance.background = UIColor.Orange;
				_alert.buttonAppearance.background = UIColor.Orange;
				_alert.buttonAppearanceHighlighted.titleColor = UIColor.Orange;
				_alert.appearance.roundBottomLeftCorner = false;

				// Respond to any button being tapped
				_alert.ButtonReleased += (button) => {
					_alert.Hide();
				};
				break;
			case 22:
				// Build a custom view for this alert
				UITextField field1 = new UITextField (new RectangleF (13, 41, 278, 29)) {
					BorderStyle = UITextBorderStyle.RoundedRect,
					TextColor = UIColor.DarkGray,
					Font = UIFont.SystemFontOfSize (14f),
					Placeholder = "Name",
					Text = "DefaultValue",
					BackgroundColor = UIColor.White,
					AutocorrectionType = UITextAutocorrectionType.No,
					KeyboardType = UIKeyboardType.Default,
					ReturnKeyType = UIReturnKeyType.Done,
					ClearButtonMode = UITextFieldViewMode.WhileEditing,
					Tag = 0,
					Enabled = true
				};
				field1.ShouldReturn = delegate (UITextField field){
					field.ResignFirstResponder ();
					return true;
				};
				field1.ShouldEndEditing= delegate (UITextField field){
					field.ResignFirstResponder ();
					return true;
				};

				// Create the new alert and attach the new field
				_alert = new UIActionAlert (UIActionAlertType.Subview, UIActionAlertLocation.Middle, "Custom Subview", field1, "Cancel,OK");
				_alert.Show ();

				// Respond to any button being tapped
				_alert.ButtonReleased += (button) => {
					field1.ResignFirstResponder ();
					_alert.Hide();
				};
				break;
			}
		}
		#endregion 

		#region Override Methods
		/// <Docs>Called when the system is running low on memory.</Docs>
		/// <summary>
		/// Dids the receive memory warning.
		/// </summary>
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		/// <summary>
		/// Views the did load.
		/// </summary>
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Set maximum stepper limit
			AlertStepper.MaximumValue = 22;

			// Show initial title
			SetAlertTitle ();

			// Respond to stepper changes
			AlertStepper.ValueChanged += (sender, e) => {
				// Show new title
				SetAlertTitle ();
			};
			
			// Display alert
			AlertButton.TouchDown += (object sender, EventArgs e) => {
				// Display new alert
				ShowAlert ();
			};
		}

		/// <summary>
		/// Shoulds the autorotate to interface orientation.
		/// </summary>
		/// <returns><c>true</c>, if autorotate to interface orientation was shoulded, <c>false</c> otherwise.</returns>
		/// <param name="toInterfaceOrientation">To interface orientation.</param>
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			if (UserInterfaceIdiomIsPhone) {
				return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
			} else {
				return true;
			}
		}
		#endregion 
	}
}

