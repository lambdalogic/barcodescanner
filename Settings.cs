using System;
using Foundation;

namespace BarcodeScanner
{
	public class Settings
	{
		public static string ServerAddress { get; private set;}
		public static string ServerToken { get; private set;}
		public static string PrintBadge { get; set;}
		public static string ProgrammePointID { get; private set;}
		public static string Placement { get; set;}
		public static string LineaPro { get; set;}

		const string serverAddressKey = "serverAddressKey";
		const string serverTokenKey = "serverTokenKey";
		const string printBadgeKey = "printBadgeKey";
		const string programmePointIDKey = "programmePointIDKey";
		const string placementKey = "placementKey";
		const string lineaProKey = "lineaProKey";

		static void LoadDefaultValues ()
		{
			var settingsDict = new NSDictionary (NSBundle.MainBundle.PathForResource ("Settings.bundle/Root.plist", null));

			var prefSpecifierArray = settingsDict[(NSString)"PreferenceSpecifiers"] as NSArray;

			foreach (var prefItem in NSArray.FromArray<NSDictionary> (prefSpecifierArray)) {
				var key = prefItem[(NSString)"Key"] as NSString;
				if (key == null)
					continue;
				var val = prefItem[(NSString)"DefaultValue"];
				switch (key.ToString ()) {
				case serverAddressKey:
					ServerAddress = val.ToString ();
				 	break;
				case printBadgeKey:
					PrintBadge = val.ToString();
					break;				
				case programmePointIDKey:
					ProgrammePointID = val.ToString ();
					break;
				case placementKey:
					Placement = val.ToString ();
					break;
				case lineaProKey:
					LineaPro = val.ToString ();
					break;
				}
			}

			var appDefaults = NSMutableDictionary.FromObjectsAndKeys (new object[] {
			
				new NSString (ServerAddress),
				new NSString (PrintBadge),
				new NSString (ProgrammePointID),
				new NSString (Placement),
				new NSString (LineaPro),
			},
				new object [] { serverAddressKey , printBadgeKey, ProgrammePointID, Placement, LineaPro,  }
			);

			NSUserDefaults.StandardUserDefaults.RegisterDefaults (appDefaults);
			NSUserDefaults.StandardUserDefaults.Synchronize ();
		}

		static void LoadServerToken ()
		{
			var settingsDict = new NSDictionary (NSBundle.MainBundle.PathForResource ("Settings.bundle/Root.plist", null));

			var prefSpecifierArray = settingsDict[(NSString)"PreferenceSpecifiers"] as NSArray;

			foreach (var prefItem in NSArray.FromArray<NSDictionary> (prefSpecifierArray)) {
				var key = prefItem[(NSString)"Key"] as NSString;
				if (key == null)
					continue;
				var val = prefItem[(NSString)"DefaultValue"];
				switch (key.ToString ()) {
				case serverTokenKey:
					ServerToken = val.ToString ();
					break;
				}
			}	
		}

		public static void SetupByPreferences ()
		{
			var _serverAddressKeyValue = NSUserDefaults.StandardUserDefaults.StringForKey (serverAddressKey);
			if (_serverAddressKeyValue == null)
				LoadDefaultValues ();
			ServerAddress = NSUserDefaults.StandardUserDefaults.StringForKey (serverAddressKey);
			PrintBadge = NSUserDefaults.StandardUserDefaults.StringForKey(printBadgeKey);
			ProgrammePointID = NSUserDefaults.StandardUserDefaults.StringForKey(programmePointIDKey);
			Placement = NSUserDefaults.StandardUserDefaults.StringForKey (placementKey);
			LineaPro = NSUserDefaults.StandardUserDefaults.StringForKey (lineaProKey);
			LoadServerToken ();
		}
	}
}

